* Name:		Alphabet Soup.
* Author(s): 	Grant Douglas & Adam McKissock.
* Contact: 	_wiresharkGD@gmail.com_.
* Python ver:	3.2

# **Description:**
The application was developed to complete the Facebook Hacker's Cup challenge 'Alphabet Soup'.
It was written very quickly and in horrible fashion as the deadline was almost up as soon as we started.

# **Usage:**
1. Change the path to your input.txt document appropriately.
2. Execute.