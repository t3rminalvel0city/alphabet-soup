#!c:/Python/python.exe -u
import array
arr=[]
letterH = 0
letterA = 0
letterC = 0
letterK = 0
letterE = 0
letterR = 0
letterU = 0
letterP = 0
#Remember and change this path to your input file.
inp = open ("C:/Users/grant/Desktop/input.txt","r")
for line in inp.readlines():
        arr.append(line)
		
value = str(arr[0:1]).replace('[',"")
value = value.replace(']',"")
value = value.replace('\\','')
value = value.replace("n","")
value = value.replace("'","")
value = value.replace("'","")
count = 0
index = 0
for element in arr:
        if count != 0:
                index += 1
                for char in element:
                        if char.find('H')!=-1:
                                letterH += 1
                        if str(char).find('A')!=-1:
                                letterA += 1
                        if str(char).find('C')!=-1:
                                letterC += 1
                        if str(char).find('K')!=-1:
                                letterK += 1
                        if str(char).find('E')!=-1:
                                letterE += 1
                        if str(char).find('R')!=-1:
                                letterR += 1
                        if str(char).find('U')!=-1:
                                letterU += 1
                        if str(char).find('P')!=-1:
                                letterP += 1
                answer = 0
                if (letterC > 1):
                        letterC = letterC / 2
                else:
                        answer = 0
       
                if letterH > 0 and letterA > 0 and letterC > 0 and letterK > 0 and letterE > 0 and letterR > 0 and letterU > 0 and letterP > 0:
                        array = [letterH, letterA, letterC, letterK, letterE, letterR, letterU, letterP]
                        answer = min(int(s) for s in array)
                letterH = 0
                letterA = 0
                letterC = 0
                letterK = 0
                letterE = 0
                letterR = 0
                letterU = 0
                letterP = 0
       
                print("Case #" + str(index) + ": " + str(answer))
        else:
                count = 1
# Close the file
inp.close()
